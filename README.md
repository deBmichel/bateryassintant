# Battery Assistant to use with (Debian / LXDE):

It is a simple battery monitor created for LXDE Desktop in debian using python; you are not going to find pythonic style or tactical pycomplications. 

The goal: Solve an individual problem with battery monitor for LXDE in debian initially.

Review the README.md and if this script function for you : happy monitor hacking.

## Problem to solve:

In LXDE Desktop for debian, you have the batery icon, but the problem for me is: when I am coding I forget the batery level and sometimes my pc shutdown suddenly and sometimes the batery is overloaded. 

I like the idea of ​​a pc running constantly without shutdown and with  a batery in good state and I know debian is very stable I have seen uptime states with 400 days, but the monitors that I found for LXDE in debian not solve my problem as I need.
		
The lifetime of batteries for laptops often decreases due to overload; sometimes recovery in debian after a fall caused by a battery discharge is not a lot of fun.
		
Thinking about this I decided to develop this little script.

# Solution proposal:

To create a batery monitor , something visual and with some sound, something not very complex , something to run in debian + LXDE very visual and significative but simple.

# Implementation idea:

1. Write a monitor in a python script that open a html result in firefox browser if the computer has a down batery level or if the batery is in overload risk.

2. Add the monitor to the crontab enabling the XDisplay option.

3. It must sound and don't compromise memory resources

# Tips for installing in crontab:
1. Check your debian firefox from terminal ""review the audio reproduction config in the browser"".

2. Clone the repo; check and test the script from terminal. Review the output and to avoid wait a specific batery level change in the script the values for constants:
		   	
* bateryminvalpercen
* baterymaxvalpercen

When you change the values use in terminal the command: python mainscript.py

You shoud see something like:
![You shoud see something like:](images/guide1.png)

or like:
![or like:](images/guide2.png)

It depends on how you play with the values ​​of the constants for the test.

But regardless of what you see or not something in firefox; you really should see an exit like this in terminal.

![A terminal result](images/guide3.png)

3. If the script really function for you, and you would like to run it constantly as simulating a little linux daemon, you can use "crontab -e + nano" but:
		    
	3.1. execute in terminal:
		chmod a+x /yourpath/bateryassintant/mainscript.py
		    
	3.2. execute in terminal:
		crontab -e    ; I suggest you use nano, or if you really know use vi.
	
	3.3. 	at the end of the crontabfile add the line:
		*/2 * * * *  cd /yourpath/bateryassintant/ && /yourpath/bateryassintant/mainscript.py > /home/devuser/Development/tools/bateryassintant/log.txt
4. In /yourpath/bateryassintant/ you should find log.txt file with the last execution info.

# Important news, considerations and questions ?:

## Memory resource compromise ?

Relatively not; LXDE is a free desktop environment with comparatively low resource requirements and the script was designed to be visual. The critical parts of resource consumption are in:

* the handling of the result file that the browser shows: If and only if necessary : The file is destroyed and created in the related execution, additinally the f.close () in 31 line mainscript.py is ensured.

* the execution via subprocess of the battery status check command: in tests with computers with Intel Core 2 duo and AMD A8 processors it turned out to be light and after its execution the memory resources were released.

* the opening of the browser: 
The script forces the firefox browser to open, if and only if the battery is running low or near overload; if you check the script you will notice that in def AnalyzePercentajeAndState (percentage, state) that is guaranteed. If and only if necessary: script open the browser with the appropriate suggestion, you must delimit the constants bateryminvalpercen and
baterymaxvalpercen.If you close the browser the memory consumed on opening should be freed.

## What about python version ?

The script run using /usr/bin/python3.7 but also runs with 3.8.2:

![Review](images/guide4.png)

review the first line of the script: #!/usr/bin/python3.7 , the good new: If you have problems with python version, you can use [pyenv](https://github.com/pyenv/pyenv) .

