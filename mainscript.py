#!/usr/bin/python3.7

"""
	It is a simple battery monitor created for LXDE Desktop in debian.
	
	Review the README.md and if this script function for you :

	happy hacking.
"""

""" --------- Begin Section: Constants. --------- """

bateryBaseCommand    = '''upower -i $(upower -e | grep 'BAT') | grep -E "state|to\ full|percentage"'''
FileAnswer           = "CheckerBateryAnswer.html"
browserCommand       = 'export DISPLAY=:0 && firefox ' + FileAnswer + '  &'
bateryminvalpercen   = 23
baterymaxvalpercen   = 93

""" --------- End   Section: Constants. --------- """

""" --------- Begin Section: Html alert manage. --------- """

import os
import datetime

def ManageAnswerRender(answer):
	if os.path.exists(FileAnswer):
		os.system('rm ' + FileAnswer)
	os.system('touch ' + FileAnswer)
	f = open(FileAnswer,"w+")	
	f.write(answer)
	f.close()
	os.system(browserCommand)

def ChargeStatus (status):	      
	return '''
		<!DOCTYPE html
		<html>
			<head>
				<script type="text/javascript">
					function CustomAlert(){
						var audio = new Audio('alarm.mp3');
						audio.play();
					}
				</script>
			</head>

			<body bgcolor="black" onload="CustomAlert()">
			  <div style="text-align: center;">
			    <a href="/">
			      <img src="images/lang-logo.png">
			    </a>
			    <h1 style="color:white;">Reviewing PC Batery</h1>
			    <h1 style="color:white;">Status:</h1>
			    ''' + status + '''
			  </div>
			</div>
			</body>
		</html>
	'''

def WarningCharge(percentage, hour):
	return ChargeStatus(
		'''
			<p style="color:white;">
			Battery Percentage: '''+str(percentage)+'''%<br />
			Date-Hour: '''+hour+'''<br />
			<h1 style="color:white;">
      			DOWN: Connect the charger !!!
      		</h1>
      		</p>
      		<br />
      		<h1 style="color:white;">And close the related Firefox browser or tab to save RAM!!</h1>
      		<br />
      		<img src="images/downBatery.jpeg">
      	'''
    )

def WarningDisCharge(percentage, hour):
	return ChargeStatus(
		'''
			<p style="color:white;">
			Battery Percentage: '''+str(percentage)+'''%<br />
			Date-Hour: '''+hour+'''<br />
			<h1 style="color:white;">
      			!!! HIGH: PLEASE DisConnect the charger NOW !!! <br />
      		</h1>
      		</p>
      		<br />
      		<h1 style="color:white;">And close the related Firefox browser or tab to save RAM!!</h1>
      		<br />
      		<img src="images/overChargingBatery.png">
      	'''
	)

""" --------- End   Section: Html alert manage. --------- """


""" --------- Begin Section: Base logic. --------- """

import subprocess

def obtainPercentageAndStateFromBatery():
	output = subprocess.check_output(bateryBaseCommand, shell=True).decode("utf-8") 

	percentage = output[output.find('percentage:'):output.find('%', output.find('percentage:'))]
	percentage = percentage.replace (' ','').replace ('percentage:','')

	state = output[output.find('state:'):output.find('\n', output.find('state:'))]
	state = state.replace (' ','').replace ('state:','')

	return percentage, state


def AnalyzePercentajeAndState(percentage, state):
	hour = datetime.datetime.now().__str__().split(".")[0]
	if int(percentage) <= bateryminvalpercen:
		if state == 'discharging':
			ManageAnswerRender(WarningCharge(int(percentage), hour))
		
	elif int(percentage) >= baterymaxvalpercen:
		if state == 'charging':
			ManageAnswerRender(WarningDisCharge(int(percentage), hour))

""" --------- End   Section: Base logic. --------- """


""" --------- Begin Section: Runner --------- """

import datetime

percentage, state = obtainPercentageAndStateFromBatery()
AnalyzePercentajeAndState(percentage, state)
print('Last Battery assistant for debian execution: \n\t' + 
	  'at:' +str(datetime.datetime.now()).split(".")[0].replace(' ', '') + '\n\t' +
	  'using comand: ' + bateryBaseCommand + '\n\t' +
	  'got: \n\t\tbatery percentage: ' + percentage + '\n\t\t' + 
	  'batery state: ' + state + '\n'
)

""" --------- End   Section: Runner --------- """	

